package ru.kmasgutova;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Task2 {

    public static void main(String[] args) throws IOException {
        List<Integer> range = IntStream.range(0, 20).boxed().collect(Collectors.toList());
        Collections.shuffle(range);
        String resultString = range.stream()
                .map(Object::toString)
                .collect(Collectors.joining(","));
        Path file = Paths.get("range.txt");
        Files.write(file, Collections.singletonList(resultString), Charset.forName("UTF-8"));
        //чтение из файла и сортировка
        String firstLine = Files.readAllLines(file).get(0);
        List<Integer> sortedList = Arrays.stream(firstLine.split(","))
                .map(Integer::valueOf)
                .sorted()
                .collect(Collectors.toList());
        System.out.println(sortedList);
        //обратная сортировка
        List<Integer> reversedList = Arrays.stream(firstLine.split(","))
                .map(Integer::valueOf)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
        System.out.println(reversedList);

    }
}
