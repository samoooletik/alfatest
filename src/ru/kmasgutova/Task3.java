package ru.kmasgutova;

import java.math.BigDecimal;

public class Task3 {
    public static void main(String[] args) {
        BigDecimal res = BigDecimal.ONE;
        for (int i = 1; i < 21; i++) {
            res = res.multiply(new BigDecimal(i));
        }
        System.out.println(res);
    }
}
